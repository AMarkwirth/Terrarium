from threading import Thread
from threading import Lock

import datetime as dt
from time import sleep
import sched

from pytz import timezone
from skyfield import almanac
from skyfield.api import N, W, wgs84, load

import sys
sys.path.append('/home/pi/Terrarium/lw14_class/')
import defs
from    lw14_class import lw14
from gpiozero import DigitalOutputDevice


#define relays
relayRain = DigitalOutputDevice(4)
relayAuricul = DigitalOutputDevice(17)
relayGrandisLight = DigitalOutputDevice(18)
relayGrandisUV = DigitalOutputDevice(27)

#set duration of rain
raintime = 40
dalibus = lw14()                                        #Create a new lw14 class
dalibus.SetI2cBus(defs.LW14_I2C_ADDRESS_1)              #Set I2C address from LW14 

myscheduler = sched.scheduler()

# Figure out local midnight.
zone = timezone('UTC')
now = zone.localize(dt.datetime.now())

midnight = now.replace(hour=0, minute=0, second=0, microsecond=0)
next_midnight = midnight + dt.timedelta(days=1)

ts = load.timescale()
t0 = ts.from_datetime(midnight)
t1 = ts.from_datetime(next_midnight)
eph = load('de421.bsp')
grandis = wgs84.latlon(14 * N, 8.9122557 * W)
auriculatus = wgs84.latlon(22.0997222 * N, 8.9122557 * W)
fGrandis = almanac.dark_twilight_day(eph, grandis)
fAuricul = almanac.dark_twilight_day(eph, auriculatus)
timesGrandis, eventsGrandis = almanac.find_discrete(t0, t1, fGrandis)
timesAuricul, eventsAuricul = almanac.find_discrete(t0, t1, fAuricul)
timeGrandisUp = (timesGrandis[3]-(23/(24*60))).utc_datetime()
timeGrandisDown = timesGrandis[4].utc_datetime()
timeAuriculUp = (timesAuricul[3]-(23/(24*60))).utc_datetime()
timeAuriculDown = timesAuricul[4].utc_datetime()
print(f'Scheduler-times Grandis: {timeGrandisUp} {timeGrandisDown}')
print(f'Scheduler-times Auricul   : {timeAuriculUp} {timeAuriculDown}')

def sunriseGrandis():
    dali_dacp(10, 1)
    sleep(20)
    relayRain.On()
    sleep(raintime)
    relayRain.off()
    for lamp in range(1,3+1):
        for brightness in range(256):
            with daliLock:
                dali_dacp(lamp, brightness)
            sleep(5*60/255)
    sleep(4*60)
    relayGrandisLight.On()
    sleep(4*60)
    relayGrandisUV.On()

def sunsetGrandis():
    relayGrandisUV.Off()    
    sleep(4*60)
    relayGrandisLight.Off()
    sleep(4*60)
    for lamp in range(1,3+1):
        for inversebrightness in range(256):
            with daliLock:
                dali_dacp(lamp, 255-inversebrightness)
            sleep(5*60/255)
            
def sunriseAuricul():
    lamp = 0
    for brightness in range(256):
        with daliLock:
            dali_dacp(lamp, brightness)
        sleep(15*60/255)
    sleep(8*60)
    relayAuricul.On()

def sunsetAuricul():
    relayAuricul.Off()
    lamp = 0
    sleep(8*60)
    for inversebrightness in range(256):
        with daliLock:
            dali_dacp(lamp, 255-inversebrightness)
        sleep(15*60/255)
    
def dali_dacp(dali_device, dali_value):
    dalibus.WaitForReady()
    dalibus.SetDaliAddress(dali_device, defs.LW14_ADR_SINGLE, defs.LW14_MODE_DACP)
    dalibus.SendData(dali_value)
        
def GrandisMorning():
    Thread(sunriseGrandis)
def GrandisEvening():
    Thread(sunsetGrandis)
def AuriculMorning():
    Thread(sunriseAuricul)
def AuriculEvening():
    Thread(sunsetAuricul)

daliLock = Lock()

myscheduler.enterabs(timeGrandisUp, 1, GrandisMorning)
myscheduler.enterabs(timeGrandisDown, 1, GrandisEvening)
myscheduler.enterabs(timeAuriculUp, 2, AuriculMorning)
myscheduler.enterabs(timeAuriculDown, 2, AuriculEvening)
print(myscheduler.queue)
myscheduler.run()
